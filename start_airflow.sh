#! /usr/bin/env sh
set -ex

# variables for MACOS only
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
export no_proxy="*"

#docker run --rm \
#       -e POSTGRES_PASSWORD=tmp_pass \
#       -e POSTGRES_USERNAME=postgres \
#       --name postgres -p5432:5432 \
#       docker.io/library/postgres:13

HOST=${HOST:-0.0.0.0}
PORT=${PORT:-9999}
LOG_LEVEL=${LOG_LEVEL:-info}

confdir="$(pwd)/data_quality/config"

export PYTHONDONTWRITEBYTECODE=1
export APP_CONF_DIR
APP_CONF_DIR="$(pwd)/data_quality/config"
export AIRFLOW_HOME
AIRFLOW_HOME="$(pwd)/airflow"
export AIRFLOW_LOGS
AIRFLOW_LOGS="$(pwd)/airflow/airflow_logs"
export PYTHONPATH
PYTHONPATH="$(pwd)"

connection_string="postgresql://user:password@localhost:5432/sample_data?&options=-csearch_path%3Dairflow_test"
export PGPASSWORD
PGPASSWORD='password'
psql -h localhost -p 5432 -d sample_data -U user -c "drop schema if exists airflow_test CASCADE"
psql -h localhost -p 5432 -d sample_data -U user -c "create schema airflow_test"
psql -h localhost -p 5432 -d sample_data -U user -a -f "airflow/ddl/ddl_airflow_test.sql"

## Env vars for Airflow
export AIRFLOW__CORE__DAGS_FOLDER
AIRFLOW__CORE__DAGS_FOLDER="$(pwd)/dags"
export AIRFLOW__CORE__SQL_ALCHEMY_CONN
AIRFLOW__CORE__SQL_ALCHEMY_CONN="${connection_string}"
export AIRFLOW__DATABASE__SQL_ALCHEMY_CONN
AIRFLOW__DATABASE__SQL_ALCHEMY_CONN="${connection_string}"
export AIRFLOW__DATABASE__SQL_ALCHEMY_SCHEMA
AIRFLOW__DATABASE__SQL_ALCHEMY_SCHEMA="airflow_test"
export AIRFLOW__CORE__KILLED_TASK_CLEANUP_TIME
AIRFLOW__CORE__KILLED_TASK_CLEANUP_TIME="3600"

# For Airflow log configuration
if [ ! -d "${AIRFLOW_LOGS}" ]; then
  mkdir "${AIRFLOW_LOGS}"
fi

mkdir -p "${AIRFLOW_LOGS}/webserver"
mkdir -p "${AIRFLOW_LOGS}/scheduler"
mkdir -p "${AIRFLOW_LOGS}/dag_processor_manager"

mkdir airflow && cp "${confdir}/airflow/airflow.cfg" airflow/

airflow db init && \
airflow users create \
         --username dev \
         --firstname dev \
         --lastname dev \
         --password dev \
         --role Admin \
         --email dev@email.com

airflow webserver \
    -A "$AIRFLOW_LOGS"/webserver/airflow-webserver.out \
    -E "$AIRFLOW_LOGS"/webserver/airflow-webserver.err \
    -l "$AIRFLOW_LOGS"/webserver/airflow-webserver.log &

airflow scheduler \
    -l "$AIRFLOW_LOGS"/scheduler/airflow-scheduler.log
