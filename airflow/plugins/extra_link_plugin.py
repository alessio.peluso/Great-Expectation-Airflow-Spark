from airflow.models.baseoperator import BaseOperatorLink
from airflow.operators.python import PythonOperator
from airflow.plugins_manager import AirflowPlugin

from dags.include.gx_python import GxPythonOperator
from airflow.models import XCom

from data_quality.read_config import CONFIG


# create the operator extra link
class HTTPDocsLink(BaseOperatorLink):

    # name the link button
    name = "GX data docs"

    # add the link button to one or more operators
    operators = [GxPythonOperator, PythonOperator]

    # function determining the link
    def get_link(self, operator, *, ti_key=None):
        gx_returned_value = (
                XCom.get_value(key="gx_returned_value", ti_key=ti_key) or
                XCom.get_value(key="return_value", ti_key=ti_key)
        )
        if gx_returned_value and gx_returned_value.get("data_docs_url", []):
            return gx_returned_value.get("data_docs_url", [])[0].get("site_url")
        else:
            return f"not_found/index.html"


# add the operator extra link to a plugin
class AirflowExtraLinkPlugin(AirflowPlugin):
    name = "gx_data_docs_plugin"
    operator_extra_links = [
        HTTPDocsLink(),
    ]