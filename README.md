# HOW TO

To run the presentation slides follow the following instructions:

```bash
npm install -g reveal-md
```

```bash
reveal-md slides/slides.md
```

Happy coding! :)
