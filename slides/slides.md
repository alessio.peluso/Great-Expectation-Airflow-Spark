---
theme : "moon"
transition: "slide"
highlightTheme: "monokai"
slideNumber: true
title: "Great Expectations - Corporate"
static-dirs: "assets"

---

[//]: # (npm install -g reveal-md)
[//]: # (cd slides)
[//]: # (reveal-md slides.md --watch)

### Corporate Data Quality 
Great Expectations

---

## Great Expectations

----

### Great Expectations
Great Expectations is a tool for validating and documenting your data.

<img src="https://docs.greatexpectations.io/assets/images/gx_oss_process-050a4264f415a1bff3ceea3ac6f9b3a0.png"></img>

----

### Key Features
<ul style="font-size: 0.75em;">
<li> <b>Expectations</b>: similar to Python unit tests' assertions
<li>  <b>Automated data profiling</b>: automates pipeline tests
<li>  <b>Data Context and Data Sources</b>: configurable connections to your data sources
<li>  <b>Data Validation</b>: validate on any batch or several batches of data with your suite of Expectations
<li>  <b>Data Docs</b>: clean, human-readable documentation
</ul>
----

#### What does Great Expectations NOT do?
<ul style="font-size: 0.75em;">
<li> <b>Great Expectations is NOT a pipeline execution framework.</b> 
  <ol> Great Expectations integrates with DAG execution tools such as <a href="https://airflow.apache.org/">Airflow</a>,
<a href="https://www.getdbt.com/">dbt</a>, <a href="https://www.prefect.io/">Prefect</a>, 
<a href="https://github.com/dagster-io/dagster">Dagster</a> and <a href="https://github.com/quantumblacklabs/kedro">Kedro</a>. 
Great Expectations does not execute your pipelines for you, but instead, validation can simply be run as a step in your pipeline.
  </ol>

<li> <b>Great Expectations is NOT a data versioning tool.</b>
<ol> Great Expectations does not store data itself. Instead, it deals in metadata about data: Expectations, Validation Results, etc. </ol> 

<li> <b>Great Expectations currently works best in a Python environment.</b>
<ol>Great Expectations is Python-based. You can invoke it from the command line without using a Python programming 
environment, but if you’re working in another ecosystem, other tools might be a better choice. </ol>

</ul>

---

## Quickstart

----

### Data validation workflow

<p style="font-size: 0.75em;">The following diagram illustrates the end-to-end GX data validation workflow.</p>

<img src="assets/gx-diagram.png"/>

----

#### Install GX

<p style="font-size: 0.75em;"> Create a python virtual env and install the requirements. </p>

```bash
python -m venv venv
source venv/bin/activate

pip install -r requirements.txt
```

<a style="font-size: 0.5em;" href="#/requirements.txt">our requirements</a>

----

#### Create a Data Context

<p style="font-size: 0.75em;"> Our data context need to address the datasources, the stores, the datadocs and all 
the store names. </p>

```python
project_config = DataContextConfig(
    config_version=3,        # gx version
    plugins_directory=None,  # plugins used
    config_variables_file_path=None,
    datasources=datasource,  # datasource config file
    stores=stores,           # stores config file
    # all the store names
    validations_store_name=CONFIG.validation_store_name,
    expectations_store_name=CONFIG.expectation_store_name,
    checkpoint_store_name=CONFIG.checkpoint_store_name,
    evaluation_parameter_store_name=CONFIG.evaluation_parameter_store_name,
    data_docs_sites=data_docs,  # datadocs config file
)
```

----

#### Create DataSources

<p style="font-size: 0.75em;"> Here we are configuring all our datasources (only Spark for us). </p>

```python
my_spark_datasource_config = DatasourceConfig(
    class_name="Datasource",  # Class name should be Datasource
    # all ExecutionEngine types can be found in GX documentation
    execution_engine={"class_name": "SparkDFExecutionEngine"}, 
    data_connectors={
        "default_runtime_data_connector_name": {  # custom name 
            # module name not to be changed  
            "module_name": "great_expectations.datasource.data_connector",
            # all class_name types can be found in GX documentation 
            # we are using RuntimeDataConnetor for IN MEMORY DATA
            "class_name": "RuntimeDataConnector",
            # custom batch identifier name
            "batch_identifiers": [
                "default_identifier_name",
            ],
        }
    },
)

datasource = {"my_spark_datasource": my_spark_datasource_config}

```

----

#### Create Stores

<p style="font-size: 0.75em;"> Here we are configuring all our stores with backend in Postgres (also backend in S3/GCS is possible). </p>

```python
from data_quality.read_config import CONFIG

stores = {
    CONFIG.store_name: {
        "class_name": "ExpectationsStore",
        "store_backend": {
            "class_name": "DatabaseStoreBackend",
            "credentials": CONFIG.credentials,  # postgres credentials
        },
    },
    CONFIG.validation_store_name: {
        "class_name": "ValidationsStore",
        "store_backend": {
            "class_name": "DatabaseStoreBackend",
            "credentials": CONFIG.credentials,  # postgres credentials
        },
    },
    "checkpoint_store": {
        "class_name": "CheckpointStore",
        "store_backend": {
            "class_name": "TupleFilesystemStoreBackend",
            # checkpoint configuration file
            "base_directory": CONFIG.checkpoint_store_base_dir,  
            "root_directory": CONFIG.root_directory,
        },
    },
    CONFIG.metric_store_name: {
        "class_name": "MetricStore",
        "store_backend": {
            "class_name": "DatabaseStoreBackend",
            "credentials": CONFIG.credentials,  # postgres credentials
        },
    },
    "evaluation_parameter_store": {"class_name": "EvaluationParameterStore"},
}

```

----
#### Create DataDocs

<p style="font-size: 0.75em;"> Here we are configuring the datastore backend, ours is local, but S3/GCS is also possible.</p>

```python
from data_quality.read_config import CONFIG

data_docs = {
    "local_site": {
        "class_name": "SiteBuilder",
        "show_how_to_buttons": False,
        "store_backend": {
            "class_name": "TupleFilesystemStoreBackend",
            # local path to datadoc folder 
            "base_directory": CONFIG.data_doc_dir, 
            "root_directory": CONFIG.root_directory,
        },
        "site_index_builder": {
            "class_name": "DefaultSiteIndexBuilder",
            "show_how_to_buttons": False,
            "show_cta_footer": False,
        },
    },
    # SAMPLE JSON TO HOST DATA DOCS IN GCS
    # "gcs_doc_site": {
    #     "class_name": "SiteBuilder",
    #     "show_how_to_buttons": "false",
    #     "store_backend": {
    #         "class_name": "TupleGCSStoreBackend",
    #         "project": "<your>",
    #         "bucket": "<your>",
    #     },
    #     "site_index_builder": {
    #         "class_name": "DefaultSiteIndexBuilder",
    #         "show_cta_footer": True,
    #     },
    # }
}

```

----

### Connect to Data
<p style="font-size: 0.75em;"> Since we are using an in memory SparkDFExecutionEngine we have to read our data in the SparkSession.</p>

```python
# READING DATA
logging.info("Starting...")
df_path: str = kwargs.get("df_name", "")
df = read_data(
    df_path=df_path,
    df_format="csv",
    options={"sep": ",", "header": True},
    df_schema=data_schema
)
logging.info(f"Data has {df.count()} rows")
```

----

### Create Expectations
<p style="font-size: 0.75em;"> There are multiple ways to create and store Expectations, 
we use the following to create them </p>

```python
# Example expectation
expectation_configuration = ExpectationConfiguration(
  expectation_type="expect_column_length_match_input_length",
  kwargs={"column": "STATION", "length": 11},
)
suite.add_expectation(
  expectation_configuration=expectation_configuration
)
```
<p style="font-size: 0.75em;"> and the following to store them in the expectations suite. </p>

```python
suite, ge_context = Expectations.create_expectations(
  suite, ge_context, CONFIG.expectation_suite_name
)
```

----

### Create Validator
<p style="font-size: 0.75em;"> We are using a <code>RuntimeBatchRequest</code> as follows:</p>

```python
batch_request = RuntimeBatchRequest(
  datasource_name="my_spark_datasource",
  data_connector_name="default_runtime_data_connector_name",
  data_asset_name="IN_MEMORY_DATA_ASSET",
  batch_identifiers={
    "default_identifier_name": "default_identifier"
  },
  # the previously read DF
  runtime_parameters={"batch_data": df}, 
)

validations = [
  {
    "batch_request": batch_request,
    "expectation_suite_name": CONFIG.expectation_suite_name,
  }
]

validator = ge_context.get_validator(
  batch_request=batch_request,
  expectation_suite_name=CONFIG.expectation_suite_name
)
```

----

### Run Checkpoint
<p style="font-size: 0.75em;"> 
With the checkpoint and the validations input we can run perform our Data Quality checks. </p>

```python
checkpoint = SimpleCheckpoint(
  name="checkpoint",
  data_context=ge_context,
  class_name="SimpleCheckpoint",
  action_list=[
    {
      "name": "store_validation_result",
      "action": {
        "class_name": "StoreValidationResultAction"
      },
    }
  ],
)

run_id = {
  "run_name": "example" + "_" + "example" + "_run",
  "run_time": datetime.datetime.now(datetime.timezone.utc),
}

checkpoint_result = checkpoint.run(
  run_id=RunIdentifier(**run_id),
  run_name_template="%Y%m%d_%H%M%S",
  validations=validations,
  action_list=[
    {
      "name": "store_validation_result",
      "action": {
        "class_name": "StoreValidationResultAction"
      },
    }
  ],
)

```

----

#### View Validation Results
<p style="font-size: 0.75em;"> 
The following method opens the saved html report of the Data Quality performed. 
</p>

```python
# OPENING DATA DOCS
ge_context.open_data_docs()
```

<img src="assets/gx-screenshot.png"/>

---

## GX in Airflow

----

### GX in Airflow (DAG)
<p style="font-size: 0.75em;"> 
Astronomer have implemented a new operator for GX <code>(GreatExpectationsOperator)</code>, but I haven't had the opportunity
to test it, so I preferred using the classic <code>PythonOperator</code>.
</p>

```python
with DAG(
    dag_id="gx-data-quality-dag",
    description="Data quality DAG using Great Expectation",
    start_date=datetime.datetime(2022, 1, 19),
    catchup=False,
    is_paused_upon_creation=False,
    tags=["data-quality", "gx"],
    params=dag_params,
    doc_md=doc_md,
) as dag:

    dq = PythonOperator(
    # run_data_quality is the 
    # ensemble method of the previous GX code blocks
        task_id="dq", 
        python_callable=run_data_quality, 
        op_kwargs={}, 
        dag=dag
    )
```

----
### GX in Airflow (Plugin)
<p style="font-size: 0.75em;"> 
With this custom Airflow plugin it is also possible to embed a new button in the task section of Airflow UI connecting 
directly with GX DataDocs.  
</p>

```python
# create the operator extra link
class HTTPDocsLink(BaseOperatorLink):

    # name the link button
    name = "GX data docs"

    # add the link button to one or more operators
    operators = [PythonOperator]

    # function determining the link
    def get_link(self, operator, *, ti_key=None):
        gx_returned_value = (
                XCom.get_value(key="gx_returned_value", ti_key=ti_key) or
                XCom.get_value(key="return_value", ti_key=ti_key)
        )
        if gx_returned_value and gx_returned_value.get("data_docs_url", []):
            return gx_returned_value.get("data_docs_url", [])[0].get("site_url")
        else:
            return f"not_found/index.html"


# add the operator extra link to a plugin
class AirflowExtraLinkPlugin(AirflowPlugin):
    name = "gx_data_docs_plugin"
    operator_extra_links = [
        HTTPDocsLink(),
    ]
```

----
### GX in Airflow (Plugin)
<p style="font-size: 0.75em;"> 
As you can see in the Extra Link section is present the "GX data docs" button </p>

<img src="assets/airflow-new-button.png"/>

---

### requirements.txt

```bash
apache-airflow==2.6.0
great-expectations>=0.15.34
sqlalchemy>=1.3.16
pendulum==2.1.2
psycopg2-binary==2.9.9
pydantic==1.10.9
pydantic-core==2.16.2
pandas>=1.1.15,<2.0.0
connexion==2.14.2
Flask-Session==0.5.0
airflow-provider-great-expectations==0.2.7
pyspark==3.4.0
```

----

### start_airflow.sh

```bash
#! /usr/bin/env sh
set -ex

# variables for MACOS only
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
export no_proxy="*"

#docker run --rm \
#       -e POSTGRES_PASSWORD=tmp_pass \
#       -e POSTGRES_USERNAME=postgres \
#       --name postgres -p5432:5432 \
#       docker.io/library/postgres:13

HOST=${HOST:-0.0.0.0}
PORT=${PORT:-9999}
LOG_LEVEL=${LOG_LEVEL:-info}

confdir="$(pwd)/data_quality/config"

export PYTHONDONTWRITEBYTECODE=1
export APP_CONF_DIR
APP_CONF_DIR="$(pwd)/data_quality/config"
export AIRFLOW_HOME
AIRFLOW_HOME="$(pwd)/airflow"
export AIRFLOW_LOGS
AIRFLOW_LOGS="$(pwd)/airflow/airflow_logs"
export PYTHONPATH
PYTHONPATH="$(pwd)"

connection_string="postgresql://user:password@localhost:5432/sample_data?&options=-csearch_path%3Dairflow_test"
export PGPASSWORD
PGPASSWORD='password'
psql -h localhost -p 5432 -d sample_data -U user -c "drop schema if exists airflow_test CASCADE"
psql -h localhost -p 5432 -d sample_data -U user -c "create schema airflow_test"
psql -h localhost -p 5432 -d sample_data -U user -a -f "airflow/ddl/ddl_airflow_test.sql"

## Env vars for Airflow
export AIRFLOW__CORE__DAGS_FOLDER
AIRFLOW__CORE__DAGS_FOLDER="$(pwd)/dags"
export AIRFLOW__CORE__SQL_ALCHEMY_CONN
AIRFLOW__CORE__SQL_ALCHEMY_CONN="${connection_string}"
export AIRFLOW__DATABASE__SQL_ALCHEMY_CONN
AIRFLOW__DATABASE__SQL_ALCHEMY_CONN="${connection_string}"
export AIRFLOW__DATABASE__SQL_ALCHEMY_SCHEMA
AIRFLOW__DATABASE__SQL_ALCHEMY_SCHEMA="airflow_test"
export AIRFLOW__CORE__KILLED_TASK_CLEANUP_TIME
AIRFLOW__CORE__KILLED_TASK_CLEANUP_TIME="3600"

# For Airflow log configuration
if [ ! -d "${AIRFLOW_LOGS}" ]; then
  mkdir "${AIRFLOW_LOGS}"
fi

mkdir -p "${AIRFLOW_LOGS}/webserver"
mkdir -p "${AIRFLOW_LOGS}/scheduler"
mkdir -p "${AIRFLOW_LOGS}/dag_processor_manager"

mkdir airflow && cp "${confdir}/airflow/airflow.cfg" airflow/

airflow db init && \
airflow users create \
         --username dev \
         --firstname dev \
         --lastname dev \
         --password dev \
         --role Admin \
         --email dev@email.com

airflow webserver \
    -A "$AIRFLOW_LOGS"/webserver/airflow-webserver.out \
    -E "$AIRFLOW_LOGS"/webserver/airflow-webserver.err \
    -l "$AIRFLOW_LOGS"/webserver/airflow-webserver.log &

airflow scheduler \
    -l "$AIRFLOW_LOGS"/scheduler/airflow-scheduler.log

```



