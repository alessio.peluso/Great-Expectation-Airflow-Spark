from typing import Any

from airflow.operators.python import PythonOperator
from airflow.utils.context import Context, context_merge


class GxPythonOperator(PythonOperator):

    # initialize with identical arguments to the parent class
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def execute(self, context: Context) -> Any:
        context_merge(context, self.op_kwargs, templates_dict=self.templates_dict)
        self.op_kwargs = self.determine_kwargs(context)

        return_value = self.execute_callable()
        if self.show_return_value_in_logs:
            self.log.info("Done. Returned value was: %s", return_value)
        else:
            self.log.info("Done. Returned value not shown")

        context["ti"].xcom_push(key="gx_returned_value", value=return_value)
        return return_value