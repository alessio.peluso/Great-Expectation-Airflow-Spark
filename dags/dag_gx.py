import datetime

from airflow.models import DAG
from include.gx_python import GxPythonOperator
from airflow.operators.python import PythonOperator
from airflow.models.param import Param

from data_quality.spark_main_GE import run_data_quality

doc_md = """
## Scope 

## How to run

"""

dag_params = {
    "df_name": Param(
        "",
        type="string",
        title="df_name",
        description="DataFrame name to apply Data Quality",
    ),
}

with DAG(
    dag_id="gx-data-quality-dag",
    description="Data quality DAG using Great Expectation",
    start_date=datetime.datetime(2022, 1, 19),
    catchup=False,
    is_paused_upon_creation=False,
    tags=["data-quality", "gx"],
    params=dag_params,
    doc_md=doc_md,
) as dag:

    dq = PythonOperator(
        task_id="dq", python_callable=run_data_quality, op_kwargs={}, dag=dag
    )

    dq_custom = GxPythonOperator(
        task_id="dq_custom", python_callable=run_data_quality, op_kwargs={}, dag=dag
    )

    dq >> dq_custom
