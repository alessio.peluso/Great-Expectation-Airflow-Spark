from data_quality.read_config import CONFIG

data_docs = {
    "local_site": {
        "class_name": "SiteBuilder",
        "show_how_to_buttons": False,
        "store_backend": {
            "class_name": "TupleFilesystemStoreBackend",
            "base_directory": CONFIG.data_doc_dir,
            "root_directory": CONFIG.root_directory,
        },
        "site_index_builder": {
            "class_name": "DefaultSiteIndexBuilder",
            "show_how_to_buttons": False,
            "show_cta_footer": False,
        },
    },
    # SAMPLE JSON TO HOST DATA DOCS IN GCS
    # "gcs_doc_site": {
    #     "class_name": "SiteBuilder",
    #     "show_how_to_buttons": "false",
    #     "store_backend": {
    #         "class_name": "TupleGCSStoreBackend",
    #         "project": "<your>",
    #         "bucket": "<your>",
    #     },
    #     "site_index_builder": {
    #         "class_name": "DefaultSiteIndexBuilder",
    #         "show_cta_footer": True,
    #     },
    # }
}
