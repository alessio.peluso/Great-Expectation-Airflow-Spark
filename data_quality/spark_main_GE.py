import datetime
import logging
import yaml

from great_expectations.core import RunIdentifier
from great_expectations.data_context import get_context

from data.schema import data_schema
from data_quality.spark_utils.spark_read import read_data
from data_quality.checkpoints.checkpoint import return_checkpoint
from data_quality.expectations.expectations import Expectations

from data_quality.read_config import CONFIG
from data_quality.data_context.data_context import project_config
from data_quality.validator.validator import (
    create_in_memory_spark_batch_request,
    return_validations,
)
from data_quality.plugins.expectations.expect_column_length_match_input_length import ExpectColumnLengthMatchInputLength


def run_data_quality(**kwargs):
    # ------------------------------------------------------------------------------ #
    # READING DATA
    logging.info("Starting...")
    df_path: str = "data/new_central_park_weather.csv"  # kwargs.get("params", {}).get("df_name", "")
    df = read_data(
        df_path=df_path,
        df_format="csv",
        options={"sep": ",", "header": True},
        df_schema=data_schema
    )
    logging.info(f"Data has {df.count()} rows")

    # ------------------------------------------------------------------------------ #
    # CREATING CONTEXT
    ge_context = get_context(project_config=project_config)
    logging.info(f"set up ge_context: {ge_context}")

    # ------------------------------------------------------------------------------ #
    # CREATING EXPECTATION SUITE
    suite = ge_context.add_or_update_expectation_suite(
        expectation_suite_name=CONFIG.expectation_suite_name
    )
    logging.info(f"set up exp-suite: {suite}")

    # ------------------------------------------------------------------------------ #
    # CREATING EXPECTATIONS
    suite, ge_context = Expectations.create_expectations(
        suite, ge_context, CONFIG.expectation_suite_name
    )
    logging.info("created expectations")
    # ------------------------------------------------------------------------------ #
    # CREATING VALIDATOR USING BATCH REQUEST
    batch_request = create_in_memory_spark_batch_request(df)

    validations = return_validations(batch_request)

    validator = ge_context.get_validator(
        batch_request=batch_request,
        expectation_suite_name=CONFIG.expectation_suite_name
    )

    # ------------------------------------------------------------------------------ #
    # LOGGER
    logging.info(validator.head())

    # ------------------------------------------------------------------------------ #
    # CREATING CHECKPOINT
    checkpoint = return_checkpoint(ge_context)
    logging.info("created checkpoint")

    run_id = {
        "run_name": "example" + "_" + "example" + "_run",
        "run_time": datetime.datetime.now(datetime.timezone.utc),
    }

    checkpoint_result = checkpoint.run(
        run_id=RunIdentifier(**run_id),
        run_name_template="%Y%m%d_%H%M%S",
        validations=validations,
        action_list=CONFIG.action_list,
    )
    logging.info("checkpoint ran")
    logging.info(f"checkpoint result: {checkpoint_result}")

    # ------------------------------------------------------------------------------ #
    # OPENING DATA DOCS
    # ge_context.open_data_docs()
    return {"data_docs_url": ge_context.get_docs_sites_urls()}


if __name__ == "__main__":
    run_data_quality()