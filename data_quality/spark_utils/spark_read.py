from typing import Optional, Dict

from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.types import StructType


def read_data(
        df_path: str,
        df_format: str,
        options: Dict,
        spark: Optional[SparkSession] = None,
        df_schema: Optional[StructType] = None,
) -> DataFrame:
    """Given a df_path returns a pyspark.DataFrame using a default spark session if not provided"""
    spark = spark if spark is not None else SparkSession.builder.getOrCreate()
    if df_schema:
        return spark.read.format(df_format).options(**options).schema(df_schema).load(df_path)
    return spark.read.format(df_format).options(**options).load(df_path)
