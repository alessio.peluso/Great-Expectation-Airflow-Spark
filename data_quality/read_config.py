import os

import yaml


class Config:

    def __init__(self, postgres_cred_path: str, postgres_conf_path: str):
        self.root_directory = os.getcwd()
        self.credentials = yaml.load(open(postgres_cred_path), Loader=yaml.Loader).get(
            "credentials", {}
        )

        self.ge_config = yaml.load(open(postgres_conf_path), Loader=yaml.Loader)

        # datasource config self.ge_config.get("datasource_name", "")
        self.datasource_name = self.ge_config.get("datasource_name", "")
        self.batch_identifier_name = self.ge_config.get("batch_identifier_name", "")

        # store config
        self.store_name = self.ge_config.get("store_name", "")
        self.validation_store_name = self.ge_config.get("validation_store_name", "")
        self.checkpoint_store_name = self.ge_config.get("checkpoint_store_name", "")
        self.checkpoint_store_base_dir = self.ge_config.get(
            "checkpoint_store_base_dir", ""
        )
        self.metric_store_name = self.ge_config.get("metric_store_name", "")
        self.expectation_store_name = self.ge_config.get("expectation_store_name", "")
        self.evaluation_parameter_store_name = self.ge_config.get(
            "evaluation_parameter_store_name", ""
        )

        # doc
        self.data_doc_dir = self.ge_config.get("data_doc_dir", "")

        # expectation suite
        self.expectation_suite_name = self.ge_config.get("expectation_suite_name", "")

        # checkpoint
        self.checkpoint_name = self.ge_config.get("checkpoint_name", "")
        self.checkpoint_config_dir = self.ge_config.get("checkpoint_config_dir", "")
        self.action_list = [
            {
                "name": "store_validation_result",
                "action": {"class_name": "StoreValidationResultAction"},
            }
        ]


CONFIG = Config(
    postgres_cred_path="data_quality/config/postgres_credentials.yaml",
    postgres_conf_path="data_quality/config/ge_config.yaml",
)
