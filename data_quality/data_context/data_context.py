from great_expectations.data_context.types.base import (
    DataContextConfig,
)

from data_quality.data_docs.data_docs import data_docs
from data_quality.datasources.spark_datasource import (
    datasource,
)
from data_quality.read_config import CONFIG
from data_quality.stores.stores import stores


project_config = DataContextConfig(
    config_version=3,
    plugins_directory=None,
    config_variables_file_path=None,
    datasources=datasource,
    stores=stores,
    validations_store_name=CONFIG.validation_store_name,
    expectations_store_name=CONFIG.expectation_store_name,
    checkpoint_store_name=CONFIG.checkpoint_store_name,
    evaluation_parameter_store_name=CONFIG.evaluation_parameter_store_name,
    data_docs_sites=data_docs,
)
