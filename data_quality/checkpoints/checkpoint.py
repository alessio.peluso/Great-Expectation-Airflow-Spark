from great_expectations.checkpoint import SimpleCheckpoint
from great_expectations.data_context import AbstractDataContext

from data_quality.read_config import CONFIG


def return_checkpoint(context: AbstractDataContext) -> SimpleCheckpoint:
    return SimpleCheckpoint(
        name="checkpoint",
        data_context=context,
        class_name="SimpleCheckpoint",
        action_list=CONFIG.action_list,
    )
