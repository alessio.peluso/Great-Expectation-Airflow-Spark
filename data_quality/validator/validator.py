from typing import List, Dict

from pyspark.sql import DataFrame
from great_expectations.core.batch import RuntimeBatchRequest

from data_quality.read_config import CONFIG


def create_in_memory_spark_batch_request(df: DataFrame) -> RuntimeBatchRequest:
    return RuntimeBatchRequest(
        datasource_name="my_spark_datasource",
        data_connector_name="default_runtime_data_connector_name",
        data_asset_name="IN_MEMORY_DATA_ASSET",
        batch_identifiers={"default_identifier_name": "default_identifier"},
        runtime_parameters={"batch_data": df},
    )


def return_validations(batch_request: RuntimeBatchRequest) -> List[Dict]:
    return [
        {
            "batch_request": batch_request,
            "expectation_suite_name": CONFIG.expectation_suite_name,
        }
    ]
