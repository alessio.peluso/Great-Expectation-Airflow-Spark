from great_expectations.core import ExpectationConfiguration

from data_quality.plugins.expectations.expect_column_length_match_input_length import ExpectColumnLengthMatchInputLength


class Expectations:

    @staticmethod
    def create_expectations(suite, context, expectation_suite_name):
        # Create an Expectation

        # TABLE EXPECTATIONS
        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_table_columns_to_match_ordered_list",
            kwargs={
                "column_list": [
                    "STATION",
                    "NAME",
                    "DATE",
                    "AWND",
                    "PRCP",
                    "SNOW",
                    "SNWD",
                    "TMAX",
                    "TMIN",
                ]
            },
        )
        # Add the Expectation to the suite
        suite.add_expectation(expectation_configuration=expectation_configuration)

        # STATION ---------------------------------------------------------------------
        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_length_match_input_length",
            kwargs={"column": "STATION", "length": 11},
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_values_to_be_of_type",
            kwargs={"column": "STATION", "type_": "StringType"},
            # Note optional comments omitted
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_values_to_not_be_null",
            kwargs={
                "column": "STATION",
                "mostly": 0.99,
            },
            meta={
                "notes": {
                    "format": "markdown",
                    "content": "Some clever comment about this expectation. **Markdown** `Supported`",
                }
            },
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_value_lengths_to_equal",
            kwargs={
                "column": "STATION",
                "value": 11,
            },
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_values_to_match_regex",
            kwargs={
                "column": "STATION",
                "regex": "[A-Z]{3}\d{8}",
            },
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        # NAME ------------------------------------------------------------------------
        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_values_to_be_of_type",
            kwargs={"column": "NAME", "type_": "StringType"},
            # Note optional comments omitted
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_values_to_not_be_null",
            kwargs={
                "column": "NAME",
                "mostly": 0.99,
            },
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_values_to_be_unique",
            kwargs={
                "column": "NAME",
                "mostly": 0.0,
            },
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        # DATE ------------------------------------------------------------------------
        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_values_to_not_be_null",
            kwargs={
                "column": "DATE",
                "mostly": 0.95,
            },
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        # AWND ------------------------------------------------------------------------
        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_values_to_not_be_null",
            kwargs={
                "column": "AWND",
                "mostly": 0.95,
            },
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_values_to_be_of_type",
            kwargs={
                "column": "AWND",
                "type_": "FloatType",
            },
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_min_to_be_between",
            kwargs={"column": "AWND", "min_value": 0, "max_value": 0.01},
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        expectation_configuration = ExpectationConfiguration(
            expectation_type="expect_column_max_to_be_between",
            kwargs={
                "column": "AWND",
                "min_value": 20,
                "max_value": 25,
                "catch_exceptions": False,
                "result_format": "SUMMARY",
                "include_config": True,
            },
        )
        suite.add_expectation(expectation_configuration=expectation_configuration)

        # -----------------------------------------------------------------------------

        context.save_expectation_suite(
            expectation_suite=suite, expectation_suite_name=expectation_suite_name
        )

        return suite, context
