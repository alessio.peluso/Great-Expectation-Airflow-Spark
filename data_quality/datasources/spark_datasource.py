from great_expectations.data_context.types.base import DatasourceConfig


my_spark_datasource_config = DatasourceConfig(
    class_name="Datasource",
    execution_engine={"class_name": "SparkDFExecutionEngine"},
    data_connectors={
        "default_runtime_data_connector_name": {
            "module_name": "great_expectations.datasource.data_connector",
            "class_name": "RuntimeDataConnector",
            "batch_identifiers": [
                "default_identifier_name",
            ],
        }
    },
)


datasource = {"my_spark_datasource": my_spark_datasource_config}
