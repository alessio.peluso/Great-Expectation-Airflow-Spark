from data_quality.read_config import CONFIG

stores = {
    CONFIG.store_name: {
        "class_name": "ExpectationsStore",
        "store_backend": {
            "class_name": "DatabaseStoreBackend",
            "credentials": CONFIG.credentials,
        },
    },
    CONFIG.validation_store_name: {
        "class_name": "ValidationsStore",
        "store_backend": {
            "class_name": "DatabaseStoreBackend",
            "credentials": CONFIG.credentials,
        },
    },
    "checkpoint_store": {
        "class_name": "CheckpointStore",
        "store_backend": {
            "class_name": "TupleFilesystemStoreBackend",
            "base_directory": CONFIG.checkpoint_store_base_dir,
            "root_directory": CONFIG.root_directory,
        },
    },
    CONFIG.metric_store_name: {
        "class_name": "MetricStore",
        "store_backend": {
            "class_name": "DatabaseStoreBackend",
            "credentials": CONFIG.credentials,
        },
    },
    "evaluation_parameter_store": {"class_name": "EvaluationParameterStore"},
}
