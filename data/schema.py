from pyspark.sql.types import (
    StringType,
    StructType,
    StructField,
    FloatType,
    DecimalType,
    IntegerType,
    DateType,
    CharType
)

data_schema = StructType(
    [
        StructField('STATION', StringType(), True),
        StructField('NAME', StringType(), True),
        StructField('DATE', DateType(), True),
        StructField('AWND', FloatType(), True),
        StructField('PRCP', FloatType(), True),
        StructField('SNOW', DecimalType(), True),
        StructField('SNWD', IntegerType(), True),
        StructField('TMAX', IntegerType(), True),
        StructField('TMIN', IntegerType(), True)
    ]
)
